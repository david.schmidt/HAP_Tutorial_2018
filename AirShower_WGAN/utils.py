import numpy as np
import tensorflow as tf
from keras.layers import Input
from keras.models import Model
from keras.layers.merge import _Merge
from keras import backend as K
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def get_session(gpu_fraction=0.40):
    ''' Allocate only a fraction of the GPU RAM - (1080 GTX 8Gb). Please do not change the fraction during the tutorial!'''
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_fraction)
    return tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


def ReadInData():
    '''Reads in the trainings data'''
    filenames = "/net/scratch/JGlombitza/Public/HAPWorkshop2018/data/Data.npz"
    data = np.load(filenames)
    return data['shower_maps'], data['Energy']


def make_trainable(model, trainable):
    ''' Freezes/unfreezes the weights in the given model for each layer'''
    for layer in model.layers:
        layer.trainable = trainable
    model.trainable = trainable


def rectangular_array(n=9):
    """ Return x,y coordinates for rectangular array with n^2 stations. """
    n0 = (n - 1.0) / 2.0
    return (np.mgrid[0:n, 0:n].astype(float) - n0)


def wasserstein_loss(y_true, y_pred):
    """Calculates the Wasserstein loss - critic maximises the distance between its output for real and generated samples.
    To achieve this generated samples have the label -1 and real samples the label 1. Multiplying the outputs by the labels results to the wasserstein loss via the Kantorovich-Rubinstein duality"""
    return K.mean(y_true * y_pred)


def gradient_penalty_loss(y_true, y_pred, averaged_batch, penalty_weight):
    """Calculates the gradient penalty (for details see arXiv:1704.00028v3).
    The 1-Lipschitz constraint for improved WGANs is enforced by adding a term to the loss which penalizes if the gradient norm in the critic unequal to 1"""
    gradients = K.gradients(K.sum(y_pred), averaged_batch)
    gradient_l2_norm = K.sqrt(K.sum(K.square(gradients)))
    gradient_penalty = penalty_weight * K.square(1 - gradient_l2_norm)
    return gradient_penalty


class RandomWeightedAverage(_Merge):
    """Takes a randomly-weighted average of two tensors"""
    def __init__(self, batch_size, *args, **kwargs):
        self.batch_size = batch_size
        super(_Merge, self).__init__(*args, **kwargs)

    def _merge_function(self, inputs):
        weights = K.random_uniform((self.batch_size, 1, 1, 1))
        return (weights * inputs[0]) + ((1 - weights) * inputs[1])


def build_generator_graph(generator, critic, latent_size):
    '''Builds the graph for training the generator part of the improved WGAN'''
    generator_in = Input(shape=(latent_size,))
    generator_out = generator([generator_in])
    critic_out = critic(generator_out)
    return Model(inputs=[generator_in], outputs=[critic_out])


def build_critic_graph(generator, critic, latent_size, batch_size=1):
    '''Builds the graph for training the critic part of the improved WGAN'''
    generator_in_critic_training = Input(shape=(latent_size,), name="noise")
    shower_in_critic_training = Input(shape=(9,9,1), name='shower_maps')
    generator_out_critic_training = generator([generator_in_critic_training])
    out_critic_training_gen = critic(generator_out_critic_training)
    out_critic_training_shower = critic(shower_in_critic_training)
    averaged_batch = RandomWeightedAverage(batch_size, name='Average')([generator_out_critic_training, shower_in_critic_training])
    averaged_batch_out = critic(averaged_batch)
    return Model(inputs=[generator_in_critic_training, shower_in_critic_training], outputs=[out_critic_training_gen, out_critic_training_shower, averaged_batch_out]), averaged_batch


def plot_loss(loss, log_dir=".", name=""):
    """Plot the traings curve"""
    fig, ax1 = plt.subplots(1, figsize=(10, 4))
    epoch = np.arange(len(loss))
    loss = np.array(loss)
    try:
        plt.plot(epoch, loss[:, 0], color='red', markersize=12, label=r'Total')
        plt.plot(epoch, loss[:, 1] + loss[:, 2], color='green', label=r'Wasserstein', linestyle='dashed')
        plt.plot(epoch, loss[:, 3], color='royalblue', markersize=12, label=r'GradientPenalty', linestyle='dashed')
    except:
        plt.plot(epoch, loss[:], color='red', markersize=12, label=r'Total')

    plt.legend(loc='upper right', prop={'size': 10})
    ax1.set_xlabel(r'Iterations')
    ax1.set_ylabel(r'Loss')
    ax1.set_ylim(-1.5, 1.5)
    fig.savefig(log_dir + '/%s_Loss.png' % name, dpi=120)
    plt.close('all')


def plot_signal_map(footprint, axis, label=None):
    """Plot a map *footprint* for an detector array specified by *v_stations*. """
    xd, yd = rectangular_array()
    filter = footprint != 0
    filter[5, 5] = True
    axis.scatter(xd[~filter], yd[~filter], c='grey', s=150, alpha=0.1, label="silent")
    circles = axis.scatter(xd[filter], yd[filter], c=footprint[filter], s = 150, alpha=1, label="loud")
    cbar = plt.colorbar(circles, ax=axis)
    cbar.set_label('signal [a.u.]')
    axis.grid(True)
    if label is not None:
        axis.text(0.95, 0.1, "Energy: %.1f EeV" % label, verticalalignment='top', horizontalalignment='right', transform=axis.transAxes, backgroundcolor='w')
    axis.set_aspect('equal')
    axis.set_xlim(-5, 5)
    axis.set_ylim(-5, 5)
    axis.set_xlabel('x [km]')
    axis.set_ylabel('y [km]')


def plot_multiple_signalmaps(footprint, log_dir='.', title='', epoch='', nrows=2, ncols=2, labels=None, shuffle=True):
    """ Plots the time and signal footprint in one figure """
    if shuffle:
        idx = np.random.choice(np.arange(footprint.shape[0]), size=nrows*ncols)
    else:
        idx = np.arange(footprint.shape[0])
    fig, sub = plt.subplots(nrows=nrows, ncols=ncols, figsize=(9, 7))
    for i in range(ncols):
        for j in range(nrows):
            try:
                plot_signal_map(footprint[idx[ncols*i+j]], axis=sub[i, j], label=labels[idx[ncols*i+j]])
            except:
                plot_signal_map(footprint[idx[ncols*i+j]], axis=sub[i, j], label=labels)
    plt.tight_layout()
    fig.subplots_adjust(left=0.02, top=0.95)
    plt.suptitle(title + ' ' + str(epoch), fontsize=12)
    fig.savefig(log_dir + '/%s_signal_maps.png' % epoch, dpi=120)
    plt.close('all')
